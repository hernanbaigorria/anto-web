<?php 
/*
Template Name: Front
*/
get_header('home'); ?>

<div class="principal_background d-flex align-items-center justify-content-center flex-wrap">
	<div class="menu-principal d-flex justify-content-center align-items-center flex-wrap">
		<a class="item-menu" href="#">
			<div class="icon-01"></div>
			<p>Trabajos</p>
		</a>
		<a class="item-menu" href="#">
			<div class="icon-02"></div>
			<p>Sobre mí</p>
		</a>
		<div class="logo-large">
			<img src="<?php bloginfo('template_url'); ?>/images/logo_large.png" class="img-fluid">
		</div>
		<a class="item-menu" href="#">
			<div class="icon-03"></div>
			<p>Contacto</p>
		</a>
		<a class="item-menu" href="#">
			<div class="icon-04"></div>
			<p>Links</p>
		</a>
	</div>
	<div class="redes-home">
		<a href="#" class="rede-h d-inline-flex justify-content-center align-items-center">
			<i class="fab fa-facebook-f"></i>
		</a>
		<a href="#" class="rede-h d-inline-flex justify-content-center align-items-center">
			<i class="fab fa-instagram"></i>
		</a>
		<a href="#" class="rede-h d-inline-flex justify-content-center align-items-center">
			<i class="fab fa-behance"></i>
		</a>
		<a href="#" class="rede-h d-inline-flex justify-content-center align-items-center">
			<i class="fab fa-linkedin-in"></i>
		</a>
		<a href="mailto:hey@fromsouthdesign.com" class="link-mail">hey@fromsouthdesign.com</a>
	</div>
	<div class="derechos">
		<p>2018 Todos los derechos reservados</p>
	</div>
</div>

<?php get_footer('home'); ?>