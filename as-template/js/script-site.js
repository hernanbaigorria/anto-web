  /* Demo purposes only */
  $("figure").mouseleave(
    function() {
      $(this).removeClass("hover");
    }
  );


jQuery(document).ready(function($) {

  /* $(".filled").imagefill(); */

  // init Masonry
  var $grid = $('.grid-wall').masonry({
    itemSelector: '.grid-item',
    percentPosition: true,
    columnWidth: '.grid-sizer'
  });

  // layout Isotope after each image loads
    

  $('.ver-toggle').click(function() {
    $(this).find('.ficha-cont').slideToggle();
  });     
  
  // initialize Isotope after all images have loaded
  var $container = $('.grid-cont').imagesLoaded( function() {
    var $container = $('.grid-cont')
    // initialize Isotope
    $container.isotope({
      itemSelector: '.thumbs-gal'
      // disable normal resizing
      // set columnWidth to a percentage of container width
    });

      // filter items when filter link is clicked
      $('.filter a').click(function(){
       var selector = jQuery(this).attr('data-filter');
       $container.isotope({ filter: selector });
       return false;  
      });  

  });       

});

jQuery(document).ready(function($) {
	

    function findBootstrapEnvironment() {
        var envs = ['xs', 'sm', 'md'];

        $el = $('<div>');
        $el.appendTo($('body'));

        for (var i = envs.length - 1; i >= 0; i--) {
            var env = envs[i];

            

            $el.addClass('hidden-'+env);
            if ($el.is(':hidden')) {
                $el.remove();
                return env
            }
        };
    }

    //example usage
    if(jQuery.browser.mobile) {
       console.log($('.zoom-ph').removeAttr('id'));
    }
 

  	$("#zoom_10").elevateZoom({
  		easing: true,
  		borderSize: 0,
  		zoomWindowWidth: 759,
  		zoomWindowHeight: 569
  	});

    
});