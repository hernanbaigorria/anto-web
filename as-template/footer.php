			</div><!-- close full-height -->
			<footer class="front">

				<div class="container">
					<div class="row">
						<div class="col-12 col-md-4">
							<p>Alejandro R. Silbestein PA <br>
							License 3376033 <br><br>

							2841 NE 185 street, unit 502 <br>
							Miami, FL 33180 <br>
							+1 (786) 356 9287 <br>
							<a href="maitlo:alejandro@mispropiedadesenmiami.com">alejandro@mispropiedadesenmiami.com</a></p>
						</div>
						<div class="col-12 col-md-4 d-flex justify-content-center align-items-center flex-wrap">
							<a href="https://www.facebook.com/MisPropiedadesEnMiami" target="_blank" class="redes d-flex justify-content-center align-items-center">
								<i class="fab fa-facebook-f"></i>
							</a>
							<a href="https://www.instagram.com/mispropiedadesenmiami/" target="_blank" class="redes d-flex justify-content-center align-items-center">
								<i class="fab fa-instagram"></i>
							</a>
							<a href="https://www.linkedin.com/company/mis-propiedades-en-miami/" target="_blank" class="redes d-flex justify-content-center align-items-center">
								<i class="fab fa-linkedin-in"></i>
							</a>
						</div>
						<div class="col-12 col-md-4 d-flex justify-content-center align-items-center flex-wrap">
							<img src="<?php bloginfo('template_url'); ?>/images/logo_foot.png" class="img-fluid">
						</div>
					</div>
				</div>

			</footer>
			<?php wp_footer(); ?>
			
	</body>
</html>