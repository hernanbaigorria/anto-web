<?php 
// SCRIPTS

function ext_files() { 

    wp_enqueue_script(
        'isotope',
        get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js',
        array( 'jquery' )
    ); 

    wp_enqueue_script(
        'masonry',
        get_stylesheet_directory_uri() . '/js/masonry.pkgd.js',
        array( 'jquery' )
    ); 

    wp_enqueue_script('flexslider_js',
        get_stylesheet_directory_uri() . '/js/flexslider/jquery.flexslider.js',
        array( 'jquery' )
    ); 
    
    wp_enqueue_style('flexslider_css',
        get_stylesheet_directory_uri() . '/js/flexslider/flexslider.css'
    );

                  

    wp_enqueue_script('zoom',
        get_stylesheet_directory_uri() . '/js/jquery.elevatezoom.js',
        array( 'jquery' )
    );  

    wp_enqueue_script('script_site',
        get_stylesheet_directory_uri() . '/js/script-site.js',
        array( 'jquery' )
    );  

}

add_action( 'wp_enqueue_scripts', 'ext_files' );

/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');


// CUSTOM POST TYPES


// Residenciales

function residenciales() {

  $labels = array(
    'name'                => _x( 'Residenciales ', 'residenciales' ),
    'singular_name'       => _x( 'Residenciales ', 'residenciales' ),
    'menu_name'           => __( 'Residenciales ', 'residenciales' ),
    'all_items'           => __( 'Todos los residenciales', 'residenciales' ),
    'add_new_item'        => __( 'Agregar nuevo residencial', 'residenciales' ),
    'add_new'             => __( 'Agregar nuevo', 'residenciales' ),
    'edit_item'           => __( 'Editar residenciales', 'residenciales' ),
    'update_item'         => __( 'Actualizar residenciales', 'residenciales' ),
    'search_items'        => __( 'Buscar residenciales', 'residenciales' ),
    'not_found'           => __( 'No se encontraron residenciales', 'residenciales' ),
    'not_found_in_trash'  => __( 'No se encontraron residenciales en la Papelera', 'residenciales' ),
  );

  $args = array( 
      'labels' => $labels,
      'hierarchical' => false,
      'has_archive' => true,
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'rewrite' => array( 'slug' => '/residenciales' ),      
      'supports' => array('title'), 
      'menu_position' => 2       
  );

  register_post_type( 'residenciales', $args );

}

// Hook into the 'init' action
add_action( 'init', 'residenciales');


function barrio() {

    $labels = array( 
        'name' => _x( 'Barrios', 'barrio' ),
        'singular_name' => _x( 'Barrios', 'barrio' ),
        'search_items' => _x( 'Buscar Barrios', 'barrio' ),
        'all_items' => _x( 'Todas las Tipo de Álbums', 'barrio' ),
        'parent_item_colon' => _x( 'Parent tipo_de_albums:', 'barrio' ),
        'edit_item' => _x( 'Editar Barrios', 'barrio' ),
        'update_item' => _x( 'Actualizar Barrios', 'barrio' ),
        'add_new_item' => _x( 'Agregar nueva Barrio', 'barrio' ),
        'new_item_name' => _x( 'Nueva Barrios', 'barrio' ),
        'separate_items_with_commas' => _x( 'Separate barrio with commas', 'barrio' ),
        'add_or_remove_items' => _x( 'Agregar o eliminar Barrio', 'barrio' ),
        'choose_from_most_used' => _x( 'Choose from the most used barrio', 'barrio' ),
        'menu_name' => _x( 'Barrios', 'barrio' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => false,
        'show_admin_column' => true, //Muestra la Columna de la Taxonomía en el Admin
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true,
        'taxonomies' => array('category')  //Muestra la Categoría  
    );



    register_taxonomy( 'barrio', array('residenciales'), $args );
}

add_action( 'init', 'barrio' );


/* /////////////////////////////////////////////////////////////////////////////////////// */


function posts_link_attributes_1() {
    return 'class="prev-post"';
}
add_filter('previous_posts_link_attributes', 'posts_link_attributes_2');

function posts_link_attributes_2() {
    return 'class="next-post"';
}
add_filter('next_posts_link_attributes', 'posts_link_attributes_1');

/*  Set custom thumbnail quality ------------ */
function crayon_thumbnail_quality( $quality ) {
    return 100;
}
add_filter( 'jpeg_quality', 'crayon_thumbnail_quality' );
add_filter( 'wp_editor_set_quality', 'crayon_thumbnail_quality' );


add_filter('login_errors', create_function('$a', "return null;"));

//Add thumbnail support
add_theme_support( 'post-thumbnails' );
add_image_size( 'img-slideshow', 1140, 590, true );
add_image_size( 'img-lookbook', 1600, 2400, true );
add_image_size( 'img-lookbook-min', 265, 397, true );
add_image_size( 'full' );

//Add menu support and register main menu
if ( function_exists( 'register_nav_menus' ) ) {
  	register_nav_menus(
  		array(
  		  'main_menu' => 'Main Menu',
        'responsive_menu' => 'Phone Menu'
  		)
  	);
}

function link_to_stylesheet() {
        if ( is_user_logged_in() ) {
        ?>
        <style type="text/css">
        	@import url(http://fonts.googleapis.com/css?family=Alegreya:400italic,700italic,900italic,400,700,900);
        	#wpadminbar { background: #E0DED2 ; position: absolute; }
        	#wpadminbar ul#wp-admin-bar-root-default>li, #wpadminbar .admin-bar-search,
        	#wpadminbar #wp-admin-bar-my-account.with-avatar>a img,
        	#wp-admin-bar-user-actions #wp-admin-bar-user-info,
        	#wp-admin-bar-user-actions #wp-admin-bar-edit-profile { display: none; }
        	#wpadminbar a.ab-item, #wpadminbar>#wp-toolbar span.ab-label, #wpadminbar>#wp-toolbar span.noticon {
        		color: #8f875f; 
        	}
        	#wpadminbar .quicklinks>ul>li>a { font-family: 'Alegreya', serif!important; font-size: 14px; padding: 0 15px 0 15px; text-align: right;}
        	#wpadminbar #wp-admin-bar-my-account.with-avatar #wp-admin-bar-user-actions>li {
        		margin: 0;
        	}
        	#wpadminbar #wp-admin-bar-user-actions.ab-submenu {
        		padding: 0 15px;
  				text-align: right;
        	}
        	#wpadminbar .quicklinks .ab-empty-item, #wpadminbar .quicklinks a, #wpadminbar .shortlink-input {
        		padding: 0;
        	}
			#wpadminbar .ab-top-menu>li.hover>.ab-item, #wpadminbar .ab-top-menu>li:hover>.ab-item, 
			#wpadminbar .ab-top-menu>li>.ab-item:focus, #wpadminbar.nojq .quicklinks .ab-top-menu>li>.ab-item:focus {
				color: #8f875f;
				background: #e0ded2;
			}
			#wpadminbar .menupop .ab-sub-wrapper, #wpadminbar .shortlink-input {
				box-shadow: none; 
				color: #8f875f;
				background: #e0ded2;
			}
			#wpadminbar .quicklinks .menupop ul li .ab-item, #wpadminbar .quicklinks .menupop ul li a strong, #wpadminbar .quicklinks .menupop.hover ul li .ab-item, #wpadminbar .shortlink-input, #wpadminbar.nojs .quicklinks .menupop:hover ul li .ab-item {
				font-family: 'Alegreya', serif!important;
				color: #8f875f;
			}		
        </style>
        <?php }
?>
<?php
}
add_action('wp_head', 'link_to_stylesheet');

// filter the Gravity Forms button type
add_filter("gform_submit_button", "form_submit_button", 10, 2);
function form_submit_button($button, $form){
    return "<button class='button btn' id='gform_submit_button_{$form["id"]}'><span>{$form['button']['text']}</span></button>";
}

// CUSTOM ADMIN GENERAL

function remove_menu_items_genearl() {
    remove_menu_page('edit.php');  // Entradas
    remove_menu_page('link-manager.php'); // Enlaces    
    remove_menu_page('edit-comments.php'); // Comentarios
}
add_action('admin_menu', 'remove_menu_items_genearl');

function change_toolbar($wp_toolbar) {  
      global $current_user; 
      $wp_toolbar->remove_node('wp-logo');  
      $wp_toolbar->remove_node('comments');
      $wp_toolbar->remove_node('new-content'); 
}  
add_action('admin_bar_menu', 'change_toolbar', 999); 


// CUSTOM DASHBOARD PANEL

function remove_screenoptions() {
  echo '<style type="text/css">#dashboard-widgets-wrap, #contextual-help-link-wrap, #screen-options-link-wrap { display: none; } </style>';
}

add_action('admin_head', 'remove_screenoptions');



function my_remove_menu_pages() {
  remove_menu_page('edit.php');  // Entradas
  //remove_menu_page('upload.php'); // Multimedia
  remove_menu_page('link-manager.php'); // Enlaces
  remove_menu_page('edit-comments.php'); // Comentarios
  //remove_menu_page('themes.php'); // Apariencia
  //remove_menu_page('plugins.php'); // Plugins
  //remove_menu_page('users.php'); // Usuarios
  //remove_menu_page('tools.php'); // Herramientas
  //remove_menu_page('options-general.php'); // Ajustes
}

add_action('admin_menu', 'my_remove_menu_pages');





// CUSTOM MENU ORDER

function custom_menu_order($menu_ord) {
  if (!$menu_ord) return true;
  
  return array(
    'index.php', // Dashboard
    'separator1', // First separator
    'edit.php', // Posts
    'upload.php', // Media
    'link-manager.php', // Links
    'edit.php?post_type=page', // Pages
    'separator2', // Second separator
    'themes.php', // Appearance
    'users.php', // Users
    'options-general.php', // Settings
    'separator-last', // Last separator
  );
}
add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
add_filter('menu_order', 'custom_menu_order');


// CUSTOM ADMIN PARA USUARIO

function remove_menu_items_for_users() {
  if( !current_user_can( 'administrator' ) ):
    remove_menu_page('edit.php');  // Entradas
    remove_menu_page('upload.php'); // Multimedia
    remove_menu_page('link-manager.php'); // Enlaces
    remove_menu_page('edit.php?post_type=page'); // Páginas
    remove_menu_page('edit.php?post_type=servicios'); // Post type ID   
    remove_menu_page('edit-comments.php'); // Comentarios
    remove_menu_page('themes.php'); // Apariencia
    remove_menu_page('plugins.php'); // Plugins
    remove_menu_page('users.php'); // Usuarios
    remove_menu_page('tools.php'); // Herramientas
    remove_menu_page('options-general.php'); // Ajustes
    remove_menu_page( 'wpcf7' ); // Contact Form 7
    remove_menu_page( 'profile.php' ); // Profile
  endif;  
}
add_action('admin_menu', 'remove_menu_items_for_users');



// Register sidebar
if ( function_exists('register_sidebar') )
    register_sidebar(array(
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
     ));

// Bootstrap_Walker_Nav_Menu setup

add_action( 'after_setup_theme', 'bootstrap_setup' );
 
if ( ! function_exists( 'bootstrap_setup' ) ):
 
	function bootstrap_setup(){
 
		add_action( 'init', 'register_menu' );
			
		function register_menu(){
			register_nav_menu( 'top-bar', 'Bootstrap Top Menu' ); 
		}
 
		class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {
 
			
			function start_lvl( &$output, $depth ) {
 
				$indent = str_repeat( "\t", $depth );
				$output	   .= "\n$indent<ul class=\"dropdown-menu\">\n";
				
			}
 
			function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
				
				$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
				$li_attributes = '';
				$class_names = $value = '';
 
				$classes = empty( $item->classes ) ? array() : (array) $item->classes;
				$classes[] = ($args->has_children) ? 'dropdown' : '';
				$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
				$classes[] = 'menu-item-' . $item->ID;
 
 
				$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
				$class_names = ' class="' . esc_attr( $class_names ) . '"';
 
				$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
				$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
 
				$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
 
				$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
				$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
				$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
				$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';
 
				$item_output = $args->before;
				$item_output .= '<a'. $attributes .'>';
				$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
				$item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
				$item_output .= $args->after;
 
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			}
 
			function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
				
				if ( !$element )
					return;
				
				$id_field = $this->db_fields['id'];
 
				//display this element
				if ( is_array( $args[0] ) ) 
					$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
				else if ( is_object( $args[0] ) ) 
					$args[0]->has_children = ! empty( $children_elements[$element->$id_field] ); 
				$cb_args = array_merge( array(&$output, $element, $depth), $args);
				call_user_func_array(array(&$this, 'start_el'), $cb_args);
 
				$id = $element->$id_field;
 
				// descend only when the depth is right and there are childrens for this element
				if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {
 
					foreach( $children_elements[ $id ] as $child ){
 
						if ( !isset($newlevel) ) {
							$newlevel = true;
							//start the child delimiter
							$cb_args = array_merge( array(&$output, $depth), $args);
							call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
						}
						$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
					}
						unset( $children_elements[ $id ] );
				}
 
				if ( isset($newlevel) && $newlevel ){
					//end the child delimiter
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
				}
 
				//end this element
				$cb_args = array_merge( array(&$output, $element, $depth), $args);
				call_user_func_array(array(&$this, 'end_el'), $cb_args);
			}
		}
 	}
endif;
?>